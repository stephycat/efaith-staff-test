<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\View\View;

use App\Models\NewsUpdate;

class HomeController extends Controller {

  public function __invoke(): View {
    return view('home', [
      'news_updates' => NewsUpdate::orderByDesc('id')->get(),
    ]);
  }

}
