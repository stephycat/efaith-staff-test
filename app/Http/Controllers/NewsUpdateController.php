<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\View\View;

use App\Models\NewsUpdate;

class NewsUpdateController extends Controller {

  public function index(): View {
    return view('news-update/index', [
      'news_updates' => NewsUpdate::orderByDesc('id')->get(),
    ]);
  }

  public function show(Request $request): View {
    return view('news-update/show', [
      'news_update' => NewsUpdate::find($request->news_update),
    ]);
  }

  public function create(): View {
    return view('news-update/create', []);
  }

  public function store(Request $request) {
    $request->validate([
      'title' => 'required',
      'content' => 'required',
      'image_url' => 'required',
      'video_url' => 'required',
    ]);

    NewsUpdate::create($request->post());

    return redirect('/');
  }

  public function edit(Request $request): View {
    return view('news-update/edit', [
      'news_update' => NewsUpdate::find($request->news_update),
    ]);
  }

  public function update(Request $request) {
    $request->validate([
      'title' => 'required',
      'content' => 'required',
      'image_url' => 'required',
      'video_url' => 'required',
    ]);

    NewsUpdate::find($request->news_update)->fill($request->post())->save();

    return redirect('/');
  }

}
