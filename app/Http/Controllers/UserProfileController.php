<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\View\View;
use App\Models\User;
use Hash;

class UserProfileController extends Controller {

  public function show(): View {
    return view('user/profile', []);
  }

  public function change_password(Request $request) {
    $request->validate([
      'old_password' => 'required',
      'new_password' => 'required|confirmed',
    ]);

    if(!Hash::check($request->old_password, auth()->user()->password)) {
      return back()->with("error", "Old Password Doesn't match!");
    }

    User::whereId(auth()->user()->id)->update([
      'password' => Hash::make($request->new_password)
    ]);

    return back()->with("status", "Password changed successfully!");
  }

}
