@extends('layouts.app')

@section('pagetitle')
  <h1>Dashboard</h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="/">Home</a></li>
      <li class="breadcrumb-item active">Dashboard</li>
    </ol>
  </nav>
@endsection

@section('content')
  <section class="section dashboard">
    <div class="row">

      <div class="col-lg-12">

        <!-- News & Updates Traffic -->
        <div class="card">
          <div class="filter">
            <a class="btn btn btn-outline-success btn-sm" href="{{ route('news-updates.create') }}">Add New</a>
            <a class="icon" href="#" data-bs-toggle="dropdown"><i class="bi bi-three-dots"></i></a>
            <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
              <li class="dropdown-header text-start">
                <h6>Filter</h6>
              </li>

              <li><a class="dropdown-item" href="#">Today</a></li>
              <li><a class="dropdown-item" href="#">This Month</a></li>
              <li><a class="dropdown-item" href="#">This Year</a></li>
            </ul>
          </div>

          <div class="card-body pb-0">
            <h5 class="card-title">
              News &amp; Updates
              <span>| Today</span>
            </h5>

            <div class="news">
              @foreach($news_updates as $news_update)
                <div class="post-item clearfix">
                  <img src="{{ $news_update->image_url }}" alt="">
                  <div style="position: fixed;">
                    <h4><a href="{{ route('news-updates.show', $news_update->id) }}">{{ $news_update->title }}</a></h4>
                    <p>{{ $news_update->content }}</p>
                  </div>
                  <div style="float: right">
                    <a class="btn btn-sm" href="{{ route('news-updates.edit', $news_update->id) }}"><i class="bi bi-pencil-square"></i></a>
                  </div>
                </div>
              @endforeach
            </div><!-- End sidebar recent posts-->

          </div>
        </div><!-- End News & Updates -->

      </div><!-- End Right side columns -->

    </div>
  </section>
@endsection
