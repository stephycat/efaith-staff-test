@extends('layouts.app')

@section('pagetitle')
  <h1></h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="/">Home</a></li>
      <li class="breadcrumb-item active">News & Updates</li>
    </ol>
  </nav>
@endsection

@section('content')
  <section class="section dashboard">
    <div class="row">

      <!-- Right side columns -->
      <div class="col-lg-12">

        <!-- News & Updates Traffic -->
        <div class="card">
          <div class="card-body">
            <h5 class="card-title">Create News Update</h5>

            <!-- Vertical Form -->
            <form action="{{ route('news-updates.store') }}" class="row g-3" method="POST">
              @csrf

              <div class="row mb-3">
                <div class="col-12">
                  <label for="title" class="form-label">Title</label>
                  <input name="title" type="text" class="form-control @error('title') is-invalid @enderror" id="title">
                </div>
              </div>

              <div class="row mb-3">
                <div class="col-12">
                  <label for="content" class="form-label">Content</label>
                  <input name="content" type="text" class="form-control @error('content') is-invalid @enderror" id="content">
                </div>
              </div>

              <div class="row mb-3">
                <div class="col-12">
                  <label for="image_url" class="form-label">Image URL</label>
                  <input name="image_url" type="url" class="form-control @error('image_url') is-invalid @enderror" id="image_url">
                </div>
              </div>

              <div class="row mb-3">
                <div class="col-12">
                  <label for="video_url" class="form-label">Video URL</label>
                  <input name="video_url" type="url" class="form-control @error('video_url') is-invalid @enderror" id="video_url">
                </div>
              </div>

              <div class="text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form><!-- Vertical Form -->

          </div>
        </div>

      </div><!-- End Right side columns -->

    </div>
  </section>
@endsection
