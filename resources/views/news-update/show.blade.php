@extends('layouts.app')

@section('pagetitle')
  <h1></h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="/">Home</a></li>
      <li class="breadcrumb-item active">News & Updates</li>
    </ol>
  </nav>
@endsection

@section('content')
  <section class="section dashboard">
    <div class="row">

      <div class="col-lg-12">

        <div class="card mb-3">
          <div class="row g-0">
            <div class="col-md-2">
              <img src="{{ $news_update->image_url }}" class="img-fluid rounded-start" alt="...">
            </div>
            <div class="col-md-8">
              <div class="card-body">
                <h5 class="card-title">{{ $news_update->title }}</h5>
                <p class="card-text">{{ $news_update->content }}</p>
								<iframe width="420" height="315"
								src="{{ $news_update->video_url }}">
								</iframe>
              </div>
            </div>
          </div>
        </div><!-- End Card with an image on left -->

      </div><!-- End Right side columns -->

    </div>
  </section>
@endsection

