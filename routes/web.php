<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\CaptchaServiceController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\NewsUpdateController;
use App\Http\Controllers\UserProfileController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', HomeController::class);

Route::get('/login', [AuthController::class, 'index'])->name('auth.index');
Route::post('/login', [AuthController::class, 'login'])->name('auth.login');
Route::get('/logout', [AuthController::class, 'logout'])->name('auth.logout');

Route::get('/captcha/reload', [CaptchaServiceController::class, 'reload']);

Route::middleware(['auth'])->group(function () {

  Route::resource('/news-updates', NewsUpdateController::class);

  Route::prefix('user-profile')->group(function () {
    Route::get('/', [UserProfileController::class, 'show'])->name('user-profile');
    Route::post('/change-password', [UserProfileController::class, 'change_password'])->name('change-password');
  });

});
